<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <title>Actualizar Llave</title>
    </head>
    <body>
        <h3>Actualizar llave</h3>
        <form action="actualizarllave" method="post" class="form-inline">
            <div class="form-group mb-2">
                <label>
                    Forma
                    <select name="forma" id="forma" class="form-control">
                        <option value="redonda"  <c:if test = "${llave.getForma() == 'redonda'}">selected="true"</c:if>>redonda</option>
                        <option value="cuadrada" <c:if test = "${llave.getForma() == 'cuadrada'}">selected="true"</c:if>>cuadrada</option>
                        <option value="ovalada"  <c:if test = "${llave.getForma() == 'ovalada'}">selected="true"</c:if>>ovalada</option>
                        </select>
                    </label>
                </div>

                <div class="form-group mb-2">

                    <label for="color">
                        color
                        <input id="color" name="color" class="form-control" value="${llave.getColor()}"/>
                </label>
            </div>

            <div class="form-group mb-2">

                <label for="marca">
                    marca
                    <input id="marca" name="marca" class="form-control" value="${llave.getMarca()}"/>
                </label>
            </div>
            <input type="hidden" value="${llave.getIdLlave()}" name="id"/>
            <input type="submit" value="Actualizar" class="btn btn-success">
        </form>
    </body>
</html>
