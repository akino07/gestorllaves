<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <title>JSP Page</title>

        <style>
            .container {
                max-width: 960px;
            }
        </style>
    </head>
    <body>
        <div class="content">
            <div class="py-5">
                <h3>Llaves registradas:</h3>
            </div>
            <div class="row">
                <div class="col-md-4 order-md-2 mb-4">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Marca</th>
                                <th>Color</th>
                                <th>Forma</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${llaves}" var="llave">
                                <tr>
                                    <td>${llave.getMarca()}</td>
                                    <td>${llave.getColor()}</td>
                                    <td>${llave.getForma()}</td>
                                    <td><a href="${ctx}/eliminarllave?id=${llave.getIdLlave()}" class="btn btn-danger btn-sm">Eliminar</a> <a href="${ctx}/actualizarllave?id=${llave.getIdLlave()}" class="btn btn-warning btn-sm">Actualizar</a></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
            <a href="${ctx}" class="btn btn-info">Inicio</a>
        </div>
    </body>
</html>