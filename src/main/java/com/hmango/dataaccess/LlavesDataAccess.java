/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hmango.dataaccess;

import com.hmango.entidades.Llave;
import java.util.List;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author akino
 */
@Stateful
public class LlavesDataAccess {

    @PersistenceContext(unitName="gestorllaves_pu")
    EntityManager em;
    
    public List<Llave> obtenerLlaves(){
        List<Llave> llaves = 
                (List<Llave>) em.createQuery("select l from Llave l", Llave.class).getResultList();
        return llaves;
    }

    public void crearLlave(Llave llave) {
        em.persist(llave);
    }

    public Llave obtenerLlavePorId(int idLlave) {
        Llave llave = em.find(Llave.class, idLlave);
        return llave;
    }
    
    public Llave obtenerLlavePorIdQuery(int idLlave){
        Llave llave = em.createQuery("select l from Llave l where l.idLlave = :id", Llave.class)
                .setParameter("id", idLlave)
                .getSingleResult();
        return llave;
    }

    public void eliminarLlavePorId(int id) {
        Llave llave = em.find(Llave.class, id);
        em.remove(llave);
    }
    
    public int eliminarLlavePorIdQuery(int id){
        return em.createQuery("DELETE FROM Llave l where l.idLlave = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    public Llave actualizarLlave(Llave llave) {
        return em.merge(llave);
    }
}
