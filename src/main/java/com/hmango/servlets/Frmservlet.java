/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hmango.servlets;

import com.hmango.dataaccess.LlavesDataAccess;
import com.hmango.entidades.Llave;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author hector
 */
@WebServlet(name = "Frmservlet", urlPatterns = {"/agregar"})
public class Frmservlet extends HttpServlet {

    @EJB
    LlavesDataAccess llavesDataAcces;
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void procesarRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        procesarRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if(request.getServletContext().getAttribute("llaves")== null){
            List<Llave> llaves= new ArrayList<>();
            request.getServletContext().setAttribute("llaves",llaves);
        
        }
        List<Llave> llaves= (List<Llave>) request.getServletContext().getAttribute("llaves");
       
        
        Llave llave= new Llave();
        llave.setColor(request.getParameter("color"));
        llave.setMarca(request.getParameter("marca"));
        llave.setForma(request.getParameter("forma"));
        
        llaves.add(llave);
        llavesDataAcces.crearLlave(llave);
        
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/llaves");
        dispatcher.forward(request, response);
        
        procesarRequest(request, response);
    }
            
}