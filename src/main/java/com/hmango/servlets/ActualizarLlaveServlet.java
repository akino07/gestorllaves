/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hmango.servlets;

import com.hmango.dataaccess.LlavesDataAccess;
import com.hmango.entidades.Llave;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author akino
 */
@WebServlet(name = "ActualizarLlaveServlet", urlPatterns = {"/actualizarllave"})
public class ActualizarLlaveServlet extends HttpServlet {
    
    @EJB
    LlavesDataAccess llavesDataAccess;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String idLlave =  request.getParameter("id");
        int id = Integer.valueOf(idLlave);
        Llave llave = llavesDataAccess.obtenerLlavePorId(id);
        request.setAttribute("llave", llave);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/detalleLlaveActualizar.jsp");
        dispatcher.forward(request, response);
        
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String idLlave =  request.getParameter("id");
        int id = Integer.valueOf(idLlave);
        
        Llave llave= new Llave();
        llave.setIdLlave(id);
        llave.setColor(request.getParameter("color"));
        llave.setMarca(request.getParameter("marca"));
        llave.setForma(request.getParameter("forma"));
        
        
        llavesDataAccess.actualizarLlave(llave);
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/llaves");
        dispatcher.forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
