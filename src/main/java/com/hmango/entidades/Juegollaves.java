/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hmango.entidades;

import java.util.List;

/**
 *
 * @author hector
 */
public class Juegollaves {
    private List<Llavero> llaveros;
    private List<Llave> llaves;
    
    
    
    public List<Llavero> getLlaveros(){
        return llaveros;
    }
    public void setLlaveros (List<Llavero> llaveros){
        this.llaveros=llaveros;
        
    }
    public List<Llave> getLlaves(){
        return llaves;
    }
    public void setLlaves(List<Llave> llaves){
        this.llaves=llaves;
    }
    public void imprimir(){
        for(Llave llave: llaves){
            System.out.println(llave.getColor());
            System.out.println(llave.getForma());
            System.out.println(llave.getMarca());
            System.out.println("------------");
           
        }
        for(Llavero llavero: llaveros){
            System.out.println(llavero.getNombre());
            System.out.println(llavero.getTamanio());
            System.out.println("------------");
        }
        
    }
    
}
